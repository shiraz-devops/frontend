FROM node:20-alpine as build-stage

WORKDIR /app

COPY package* .

RUN npm install

COPY . .

RUN npm run build

FROM nginx:1.25-alpine as production-stage

WORKDIR /app

COPY --from=build-stage /app/dist /app

COPY nginx.conf /etc/nginx/nginx.conf

EXPOSE 80

CMD nginx -g "daemon off;"